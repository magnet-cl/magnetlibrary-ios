# MNCLLibrary

[![CI Status](http://img.shields.io/travis/benjamín fantini/MNCLLibrary.svg?style=flat)](https://travis-ci.org/benjamín fantini/MNCLLibrary)
[![Version](https://img.shields.io/cocoapods/v/MNCLLibrary.svg?style=flat)](http://cocoadocs.org/docsets/MNCLLibrary)
[![License](https://img.shields.io/cocoapods/l/MNCLLibrary.svg?style=flat)](http://cocoadocs.org/docsets/MNCLLibrary)
[![Platform](https://img.shields.io/cocoapods/p/MNCLLibrary.svg?style=flat)](http://cocoadocs.org/docsets/MNCLLibrary)

## Usage

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

MNCLLibrary is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

    pod "MNCLLibrary"

## Author

benjamín fantini, bdfantini@gmail.com

## License

MNCLLibrary is available under the MIT license. See the LICENSE file for more info.

