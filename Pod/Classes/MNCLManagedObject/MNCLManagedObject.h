//
//  MNCLManagedObject.h
//  Pods
//
//  Created by Magnet on 03-02-15.
//
//

#import <CoreData/CoreData.h>

@interface MNCLManagedObject : NSManagedObject

@property (strong, nonatomic) NSNumber *serverId;

@end
