//
//  MNCLApiManager.h
//  Pods
//
//  Created by Magnet on 05-02-15.
//
//

#import <Foundation/Foundation.h>

#import "MNCLApiConnector.h"

@protocol MNCLApiManagerProtocol <NSObject>

- (NSString*)apiRelativeUrlString;

@end

@interface MNCLApiManager : NSObject <MNCLApiManagerProtocol>

@property (strong, nonatomic) MNCLApiConnector *apiConnector;
@property (strong, nonatomic) id <MNCLApiConnectorDelegate> delegate;

@end
