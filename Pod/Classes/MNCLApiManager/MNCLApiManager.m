//
//  MNCLApiManager.m
//  Pods
//
//  Created by Magnet on 05-02-15.
//
//

#import "MNCLApiManager.h"

@implementation MNCLApiManager

- (id)init {
    self = [super init];
    if (self) {
        self.apiConnector = [[MNCLApiConnector alloc] initWithUrlString:[self apiRelativeUrlString]];
        
    }
    return self;
}

# pragma mark - MNCLApiManagerProtocol

- (NSString *)apiRelativeUrlString {
    return @"";
}

@end
