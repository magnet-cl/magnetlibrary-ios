//
//  MNCLApiConnector.h
//  Pods
//
//  Created by Magnet on 05-02-15.
//
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@protocol MNCLApiConnectorDelegate <NSObject>

- (NSString*)apiConnectorBaseUrl;

@end

@interface MNCLApiConnector : NSObject

@property (nonatomic, strong) id <MNCLApiConnectorDelegate> appDelegate;

@property (nonatomic, strong) NSString                      *baseUrlString;
@property (nonatomic, strong) NSMutableString               *endPointUrlString;
@property (nonatomic, strong) AFHTTPRequestOperationManager *requestOperationManager;

- (id)initWithUrlString:(NSString*)relativeUrlString;

@end
