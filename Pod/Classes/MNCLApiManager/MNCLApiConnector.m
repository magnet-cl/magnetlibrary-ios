//
//  MNCLApiConnector.m
//  Pods
//
//  Created by Magnet on 05-02-15.
//
//

#import "MNCLApiConnector.h"

@implementation MNCLApiConnector

- (id)init {
    self = [super init];
    if (self) {
        // Init with app delegate api base url configuration
        self.baseUrlString = [self.appDelegate apiConnectorBaseUrl];
        self.endPointUrlString = [NSMutableString stringWithString:self.baseUrlString];
        
        // Init the default manager
        self.requestOperationManager = [AFHTTPRequestOperationManager manager];
        
        // Set Json serializers as default.
        [self.requestOperationManager setRequestSerializer:[AFJSONRequestSerializer serializer]];
        [self.requestOperationManager setResponseSerializer:[AFJSONResponseSerializer serializer]];
        
    }
    return self;
}

- (id)initWithUrlString:(NSString*)relativeUrlString {
    self = [self init];
    if (self) {
        [self.endPointUrlString appendString:relativeUrlString];
    }
    return self;
}

@end
