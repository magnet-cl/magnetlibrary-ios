
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// AFNetworking
#define COCOAPODS_POD_AVAILABLE_AFNetworking
#define COCOAPODS_VERSION_MAJOR_AFNetworking 2
#define COCOAPODS_VERSION_MINOR_AFNetworking 5
#define COCOAPODS_VERSION_PATCH_AFNetworking 0

// AFNetworking/NSURLConnection
#define COCOAPODS_POD_AVAILABLE_AFNetworking_NSURLConnection
#define COCOAPODS_VERSION_MAJOR_AFNetworking_NSURLConnection 2
#define COCOAPODS_VERSION_MINOR_AFNetworking_NSURLConnection 5
#define COCOAPODS_VERSION_PATCH_AFNetworking_NSURLConnection 0

// AFNetworking/NSURLSession
#define COCOAPODS_POD_AVAILABLE_AFNetworking_NSURLSession
#define COCOAPODS_VERSION_MAJOR_AFNetworking_NSURLSession 2
#define COCOAPODS_VERSION_MINOR_AFNetworking_NSURLSession 5
#define COCOAPODS_VERSION_PATCH_AFNetworking_NSURLSession 0

// AFNetworking/Reachability
#define COCOAPODS_POD_AVAILABLE_AFNetworking_Reachability
#define COCOAPODS_VERSION_MAJOR_AFNetworking_Reachability 2
#define COCOAPODS_VERSION_MINOR_AFNetworking_Reachability 5
#define COCOAPODS_VERSION_PATCH_AFNetworking_Reachability 0

// AFNetworking/Security
#define COCOAPODS_POD_AVAILABLE_AFNetworking_Security
#define COCOAPODS_VERSION_MAJOR_AFNetworking_Security 2
#define COCOAPODS_VERSION_MINOR_AFNetworking_Security 5
#define COCOAPODS_VERSION_PATCH_AFNetworking_Security 0

// AFNetworking/Serialization
#define COCOAPODS_POD_AVAILABLE_AFNetworking_Serialization
#define COCOAPODS_VERSION_MAJOR_AFNetworking_Serialization 2
#define COCOAPODS_VERSION_MINOR_AFNetworking_Serialization 5
#define COCOAPODS_VERSION_PATCH_AFNetworking_Serialization 0

// AFNetworking/UIKit
#define COCOAPODS_POD_AVAILABLE_AFNetworking_UIKit
#define COCOAPODS_VERSION_MAJOR_AFNetworking_UIKit 2
#define COCOAPODS_VERSION_MINOR_AFNetworking_UIKit 5
#define COCOAPODS_VERSION_PATCH_AFNetworking_UIKit 0

// Expecta
#define COCOAPODS_POD_AVAILABLE_Expecta
#define COCOAPODS_VERSION_MAJOR_Expecta 0
#define COCOAPODS_VERSION_MINOR_Expecta 3
#define COCOAPODS_VERSION_PATCH_Expecta 1

// Expecta+Snapshots
#define COCOAPODS_POD_AVAILABLE_Expecta_Snapshots
#define COCOAPODS_VERSION_MAJOR_Expecta_Snapshots 1
#define COCOAPODS_VERSION_MINOR_Expecta_Snapshots 3
#define COCOAPODS_VERSION_PATCH_Expecta_Snapshots 1

// FBSnapshotTestCase
#define COCOAPODS_POD_AVAILABLE_FBSnapshotTestCase
#define COCOAPODS_VERSION_MAJOR_FBSnapshotTestCase 1
#define COCOAPODS_VERSION_MINOR_FBSnapshotTestCase 5
#define COCOAPODS_VERSION_PATCH_FBSnapshotTestCase 0

// MBProgressHUD
#define COCOAPODS_POD_AVAILABLE_MBProgressHUD
#define COCOAPODS_VERSION_MAJOR_MBProgressHUD 0
#define COCOAPODS_VERSION_MINOR_MBProgressHUD 9
#define COCOAPODS_VERSION_PATCH_MBProgressHUD 0

// MNCLLibrary
#define COCOAPODS_POD_AVAILABLE_MNCLLibrary
#define COCOAPODS_VERSION_MAJOR_MNCLLibrary 0
#define COCOAPODS_VERSION_MINOR_MNCLLibrary 1
#define COCOAPODS_VERSION_PATCH_MNCLLibrary 0

// ObjectiveRecord
#define COCOAPODS_POD_AVAILABLE_ObjectiveRecord
#define COCOAPODS_VERSION_MAJOR_ObjectiveRecord 1
#define COCOAPODS_VERSION_MINOR_ObjectiveRecord 5
#define COCOAPODS_VERSION_PATCH_ObjectiveRecord 0

// ObjectiveSugar
#define COCOAPODS_POD_AVAILABLE_ObjectiveSugar
#define COCOAPODS_VERSION_MAJOR_ObjectiveSugar 1
#define COCOAPODS_VERSION_MINOR_ObjectiveSugar 1
#define COCOAPODS_VERSION_PATCH_ObjectiveSugar 0

// Specta
#define COCOAPODS_POD_AVAILABLE_Specta
// This library does not follow semantic-versioning,
// so we were not able to define version macros.
// Please contact the author.
// Version: 0.3.0.beta1.

