//
//  main.m
//  MNCLLibrary
//
//  Created by benjamín fantini on 02/05/2015.
//  Copyright (c) 2014 benjamín fantini. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MNCLAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([MNCLAppDelegate class]));
    }
}
