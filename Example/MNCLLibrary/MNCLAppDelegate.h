//
//  MNCLAppDelegate.h
//  MNCLLibrary
//
//  Created by CocoaPods on 02/05/2015.
//  Copyright (c) 2014 benjamín fantini. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MNCLAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
